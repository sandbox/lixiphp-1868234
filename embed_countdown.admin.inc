<?php

function embed_countdown_admin_content() {
  $SQL = "SELECT * FROM {embed_countdown_channels}";
  $limit = 20;
  $header = array(
    array('data' => t('ID'), 'field' => 'channel_id'),
    array('data' => t('Subject')),
    array('data' => t('Status'), 'field' => 'status'),
    array('data' => t('Duration'), 'field' => 'duration'),
    array('data' => t('Created'), 'field' => 'created'),
    array('data' => t('Updated'), 'field' => 'updated'),
    array('data' => t('Functions')),
  );
  $tablesort = tablesort_sql($header);

  $result = pager_query($SQL . $tablesort, $limit, 0, NULL);
	
  $rows = array();
  while ($data = db_fetch_object($result)) {
    $data->duration = $data->duration / 60;
    $hour = floor($data->duration / 60);
    $minute = ($data->duration % 60);
    $row = array();
    $row[] = array('data' => $data->channel_id, 'class' => 'username-cell');
    $row[] = array('data' => l($data->title, 'admin/content/countdown/' . $data->channel_id), 'class' => 'title-cell');
    $row[] = array('data' => ($data->status ? t('Enable') : t('Disable') ), 'class' => 'username-cell');
    $row[] = array('data' => $hour.'hrs '.$minute.'mins', 'class' => 'username-cell');
    $row[] = array('data' => date('m/d/Y h:i A', $data->created), 'class' => 'date-cell');
    $row[] = array('data' => date('m/d/Y h:i A', $data->updated), 'class' => 'date-cell');
    $links = l(t('Export'), 'admin/content/countdown/' . $data->channel_id.'/export');
    $links .= ' | ' . l(t('Results'), 'admin/content/countdown/' . $data->channel_id.'/result/list');
    $links .= ' | ' . l(t('Edit'), 'admin/content/countdown/' . $data->channel_id.'/edit');
    $links .= (user_access('delete channels')) ? ' | ' . l(t('Delete'), 'admin/content/countdown/' . $data->channel_id . '/delete') : '';
    $links .= (user_access('administer channel results')) ? ' | ' . l(t('Clear'), 'admin/content/countdown/' . $data->channel_id . '/result/clear') : '';
    $row[] = array('data' => $links, 'class' => 'username-cell');
    $rows[] = $row;
  }    
  if (!$rows) {
    $rows[] = array(array('data' => t('No results'), 'colspan' => 7));
  }
	
  $output .= theme('table', $header, $rows, $attributes = array('id' => 'channel_list_table'));
  $output .= theme('pager', NULL, $limit, 0);

  return $output;
}

function embed_countdown_create_channel_form(&$form_state) {
  $form = array();

  $form['description'] = array(
    '#value' => t('Use this form to create a new channel.'),
  );

  $form['new_channel'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create a new channel'),
  );

  $form['new_channel']['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => '',
    '#size' => 80,
    '#maxlength' => 120,
    '#required' => TRUE,
  );

  $templates = array(
    1 => t('Enable'),
    0 => t('Disable'),
  );

  $form['new_channel']['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#default_value' => 1,
    '#options' => $templates, 
  );
  
  $form['new_channel']['duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration Minutes'),
    '#default_value' => '',
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => TRUE,
    '#description' => t('Please input the time format with integer minutes'),
  );
  $form['new_channel']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiry URL Redirection'),
    '#default_value' => '',
    '#size' => 80,
    '#maxlength' => 120,
    '#description' => t('Keep empty will refresh current page, otherwise input url with \'http://\''),
  );

  $form['new_channel']['prefix'] = array(
    '#type' => 'textarea',
    '#title' => t('Before countdown text'),
    '#required' => FALSE,
    '#rows' => 8,
  );
  
  $form['new_channel']['suffix'] = array(
    '#type' => 'textarea',
    '#title' => t('After countdown text'),
    '#required' => FALSE,
    '#rows' => 8,
  );
  
  $form['new_channel']['expiry_prefix'] = array(
    '#type' => 'textarea',
    '#title' => t('Before expiry countdown text'),
    '#required' => FALSE,
    '#rows' => 8,
  );
  
  $form['new_channel']['expiry_suffix'] = array(
    '#type' => 'textarea',
    '#title' => t('After expiry countdown text'),
    '#required' => FALSE,
    '#rows' => 8,
  );

  $form['new_channel']['submit'] = array('#type' => 'submit', '#value' => t('Submit new channel'));

  $form['#validate'] = array('embed_countdown_create_validate');

  return $form;

}

function embed_countdown_create_validate($form, &$form_state) {
  if ($form_state['values']['duration'] != '' && intval($form_state['values']['duration']) < 1 ) {
    form_set_error('duration', t('That duration time must be positive integer.'));
  }
}

function embed_countdown_create_channel_form_submit($form, &$form_state) {
  global $user;
  $time = time();
  $prefix = $form_state['values']['prefix'];
  $suffix = $form_state['values']['suffix'];
  $status = $form_state['values']['status'];
  $duration = $form_state['values']['duration'];
  $subject = $form_state['values']['subject'];
  $url = $form_state['values']['url'];
  $expiry_prefix = $form_state['values']['expiry_prefix'];
  $expiry_suffix = $form_state['values']['expiry_suffix'];
  $duration = $duration * 60;

  //create ticket
  $SQL = "INSERT INTO {embed_countdown_channels} (created, status, updated, title, url, duration, prefix, suffix, expiry_prefix, expiry_suffix) VALUES (%d, %d, %d, '%s', '%s', %d, '%s', '%s', '%s', '%s')";
  db_query($SQL, $time, $status, $time, $subject, $url, $duration, $prefix, $suffix, $expiry_prefix, $expiry_suffix);
  $channel_id = db_last_insert_id('embed_countdown_channels', 'channel_id');

  drupal_set_message(t('Channel added.'));
  $form_state['redirect'] = 'admin/content/countdown/list';
}

function embed_countdown_export_channel_form(&$form_state, $channel_id) {
  $form = array();
  
  $form['description'] = array(
    '#value' => t('Copy this code to your node content of this site.'),
  );
  $form['copy'] = array(
    '#type' => 'fieldset',
    '#title' => t('Copy code from channel'),
  );
  $form['copy']['content'] = array(
    '#type' => 'textarea',
    '#title' => t('Content'),
    '#required' => FALSE,
    '#rows' => 8,
    '#default_value' => '<script type="text/javascript" src="'.url('ajax/countdown/'.$channel_id).'"></script>',
    '#description' => 'Copy this code to your node content of this site.',
  );
  $form['return'] = array(
    '#value' => '<div id="return_link">' . l(t('Go back to channel overview.'), 'admin/content/countdown/list') . '</div>',
  );
  return $form;
}

function embed_countdown_edit_channel_form(&$form_state, $channel_id) {
  $form = array();
  $SQL = "SELECT * FROM {embed_countdown_channels} WHERE channel_id = %d";
  $result = db_query($SQL, $channel_id);

  $channel = db_fetch_object($result);
  
  $form['description'] = array(
    '#value' => t('Use this form to edit channel.'),
  );

  $form['edit'] = array(
    '#type' => 'fieldset',
    '#title' => t('Edit channel'),
  );

  $form['edit']['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => '',
    '#size' => 80,
    '#maxlength' => 120,
    '#required' => TRUE,
    '#default_value' => $channel->title,
  );

  $templates = array(
    1 => t('Enable'),
    0 => t('Disable'),
  );

  $form['edit']['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#default_value' => $channel->status,
    '#options' => $templates, 
  );
  
  $form['edit']['duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration Minutes'),
    '#default_value' => ($channel->duration / 60),
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => TRUE,
    '#description' => t('Please input the time format with integer minutes'),
  );
  $form['edit']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiry URL Redirection'),
    '#default_value' => '',
    '#size' => 80,
    '#maxlength' => 120,
    '#default_value' => $channel->url,
    '#description' => t('Keep empty will refresh current page, otherwise input url with \'http://\''),
  );

  $form['edit']['prefix'] = array(
    '#type' => 'textarea',
    '#title' => t('Before countdown text'),
    '#required' => FALSE,
    '#rows' => 8,
    '#default_value' => $channel->prefix,
  );
  
  $form['edit']['suffix'] = array(
    '#type' => 'textarea',
    '#title' => t('After countdown text'),
    '#required' => FALSE,
    '#rows' => 8,
    '#default_value' => $channel->suffix,
  );
  
  $form['edit']['expiry_prefix'] = array(
    '#type' => 'textarea',
    '#title' => t('Before expiry countdown text'),
    '#required' => FALSE,
    '#rows' => 8,
    '#default_value' => $channel->expiry_prefix,
  );
  
  $form['edit']['expiry_suffix'] = array(
    '#type' => 'textarea',
    '#title' => t('After expiry countdown text'),
    '#required' => FALSE,
    '#rows' => 8,
    '#default_value' => $channel->expiry_suffix,
  );

  $form['edit']['channel_id'] = array('#type' => 'hidden', '#value' => $channel->channel_id);
  $form['edit']['submit'] = array('#type' => 'submit', '#value' => t('Submit changes'));

  $form['#validate'] = array('embed_countdown_create_validate');

  return $form;
}

function embed_countdown_edit_channel_form_submit($form, &$form_state) {
  global $user;
  $time = time();
  $prefix = $form_state['values']['prefix'];
  $suffix = $form_state['values']['suffix'];
  $status = $form_state['values']['status'];
  $duration = $form_state['values']['duration'];
  $subject = $form_state['values']['subject'];
  $url = $form_state['values']['url'];
  $expiry_prefix = $form_state['values']['expiry_prefix'];
  $expiry_suffix = $form_state['values']['expiry_suffix'];
  $id = $form_state['values']['channel_id'];
  $duration = $duration * 60;

  //create ticket
  $SQL = "UPDATE {embed_countdown_channels} SET status = %d, updated = %d, title = '%s', url = '%s', duration = %d, prefix = '%s', suffix = '%s', expiry_prefix = '%s', expiry_suffix = '%s' WHERE channel_id = %d";
  db_query($SQL, $status, $time, $subject, $url, $duration, $prefix, $suffix, $expiry_prefix, $expiry_suffix, $id);
  
  db_query("UPDATE {embed_countdown_data} SET ended = created+%d WHERE channel_id = %d", $duration, $id);
  
  $cid_parts = array();
  $cid_parts[] = 'embed_countdown';
  $cid_parts[] = 'channel_id';
  $cid_parts[] = $id;
  $cid = implode(':', $cid_parts);
  cache_clear_all($cid, 'cache');
  
  drupal_set_message(t('Changes saved.'));
  $form_state['redirect'] = 'admin/content/countdown/list';
}

function embed_countdown_delete_channel_form(&$form_state, $channel_id) {
  $form = array();

  $subject = db_result(db_query("SELECT title FROM {embed_countdown_channels} WHERE channel_id = %d", $channel_id));
  
  $form['channel_id'] = array('#type' => 'hidden', '#value' => $channel_id);
  
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $subject)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/content/countdown/list',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute node deletion
 */
function embed_countdown_delete_channel_form_submit($form, &$form_state) {
  $channel_id = $form_state['values']['channel_id'];
  if ($form_state['values']['confirm']) {
    $SQL = "DELETE {embed_countdown_channels}, {embed_countdown_data} FROM {embed_countdown_channels}, {embed_countdown_data} WHERE embed_countdown_channels.channel_id = %d AND embed_countdown_data.channel_id = %d";
    db_query($SQL, $channel_id, $channel_id);
    
    $cid_parts = array();
    $cid_parts[] = 'embed_countdown';
    $cid_parts[] = 'channel_id';
    $cid_parts[] = $channel_id;
    $cid = implode(':', $cid_parts);
    cache_clear_all($cid, 'cache');
    
    drupal_set_message(t('Channel @channel_id deleted.', array('@channel_id' => $channel_id)));
  }

  $form_state['redirect'] = 'admin/content/countdown/list';
}

function embed_countdown_results_form($channel_id) {
  $SQL = "SELECT * FROM {embed_countdown_data} where channel_id = $channel_id";
  $limit = 20;
  $ip = arg(6);
  if($ip) {
    $where = ' and remote_addr="'.$ip.'" ';
  } else {
    $where = '';
  }
  $header = array(
    array('data' => t('ID'), 'field' => 'result_id'),
    array('data' => t('Created'), 'field' => 'created'),
    array('data' => t('Ended'), 'field' => 'ended'),
    array('data' => t('Remote IP'), 'field' => 'remote_addr'),
    array('data' => t('Functions')),
  );
  $tablesort = tablesort_sql($header);

  $result = pager_query($SQL . $where . $tablesort, $limit, 0, NULL);

  $rows = array();
  while ($data = db_fetch_object($result)) {
    $hour = floor($data->duration / 60);
    $minute = ($data->duration % 60);
    $row = array();
    $row[] = array('data' => $data->result_id, 'class' => 'username-cell');
    $row[] = array('data' => date('m/d/Y h:i A', $data->created), 'class' => 'date-cell');
    $row[] = array('data' => date('m/d/Y h:i A', $data->ended), 'class' => 'date-cell');
    $row[] = array('data' => $data->remote_addr, 'class' => 'date-cell');
    $links = (user_access('administer channel results')) ? l(t('Delete'), 'admin/content/countdown/' . $data->result_id . '/result/delete') : '';
    $row[] = array('data' => $links, 'class' => 'username-cell');
    $rows[] = $row;
  }    
  if (!$rows) {
    $rows[] = array(array('data' => t('No results'), 'colspan' => 5));
  }
	
  $output .= '<h2>Your current IP is: '.$_SERVER["REMOTE_ADDR"].'</h2>';
  $output .= 'Filter by IP: <input type="textfield" name="ip" id="s_ip" value="'.$ip.'" />&nbsp; 
  <input type="button" class="form-submit" value="'.t('Filter').'" id="filter_ip" name="op">';
  if($ip) {
    $output .= '<input type="button" class="form-submit" value="'.t('Reset').'" id="ip_reset" name="op">';
  }
  
  $output .= theme('table', $header, $rows, $attributes = array('id' => 'channel_result_table'));
  $output .= theme('pager', NULL, $limit, 0);
  
  drupal_add_js('$(function(){$("#filter_ip").click(function(){if($("#s_ip").val() == "") {alert("Please input IP."); return false;} window.location="'.url('admin/content/countdown/'.$channel_id.'/result/list/').'"+$("#s_ip").val();});$("#ip_reset").click(function(){window.location="'.url('admin/content/countdown/'.$channel_id.'/result/list').'"});});', 'inline');

  return $output;
}

function embed_countdown_clear_results_form(&$form_state, $channel_id) {
  drupal_set_title(t('Clear Channel Results'));

  $form = array();
  $form['channel_id'] = array('#type' => 'hidden', '#value' => $channel_id);
  $question = t('Are you sure you want to delete all results for this channel?');

  return confirm_form($form, $question, 'admin/content/countdown/' . $channel_id . '/result', NULL, t('Clear'), t('Cancel'));
}

function embed_countdown_clear_results_form_submit($form, &$form_state) {
  $channel_id = $form_state['values']['channel_id'];
  if ($form_state['values']['confirm']) {
    $SQL = "DELETE FROM {embed_countdown_data} WHERE channel_id = %d";
    
    db_query($SQL, $channel_id);
    
    drupal_set_message(t('Channel @channel_id cleared results.', array('@channel_id' => $channel_id)));
  }

  $form_state['redirect'] = 'admin/content/countdown/' . $channel_id . '/result';
}

function embed_countdown_delete_results_form(&$form_state, $result_id) {
  drupal_set_title(t('Delete Channel Results'));
  
  $SQL = "SELECT channel_id FROM {embed_countdown_data} where result_id = %d";
  $channel_id = db_result(db_query($SQL, $result_id));
  $form = array();
  
  $form['result_id'] = array('#type' => 'hidden', '#value' => $result_id);
  $question = t('Are you sure you want to delete this result?');

  return confirm_form($form, $question, 'admin/content/countdown/' . $channel_id . '/result', NULL, t('Delete'), t('Cancel'));
}

function embed_countdown_delete_results_form_submit($form, &$form_state) {
  $result_id = $form_state['values']['result_id'];
  $SQL = "SELECT channel_id FROM {embed_countdown_data} where result_id = %d";
  $channel_id = db_result(db_query($SQL, $result_id));
  
  if ($form_state['values']['confirm']) {
    $SQL = "DELETE FROM {embed_countdown_data} WHERE result_id = %d";
    
    db_query($SQL, $result_id);
    
    drupal_set_message(t('Channel @channel_id result @result_id removed.', array('@channel_id'=> $channel_id, '@result_id' => $result_id)));
  }

  $form_state['redirect'] = 'admin/content/countdown/' . $channel_id . '/result';
}